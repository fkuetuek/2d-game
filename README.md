# 2D Game

Tested with OpenJDK 11 (Windows10 and Ubuntu 20.04)

### Game Controls

`Up and down arrow keys to move the player.`\
`Create a bubble shield: x key`

### Run the Game

`2DGame.jar`


*Note: This game is developed for Computer Graphics course.*