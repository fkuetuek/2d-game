package game;

import java.awt.Color;

/*
 * The Config class is used for configuring the game
 * 
 * @author	fatma
 * @version	1.0
 * @since	2021-11-01
 * */

public class Config {

	public static final boolean DEBUG = false;
	public static final int UPS = 30;
	public static final int FPS = 30;
	public static final int TILE_SIZE = 24*5;
	public static final int GRID_X = 6*2;
	public static final int GRID_Y = 4*2;
	public static final int WINDOW_WIDTH = TILE_SIZE*GRID_X; 	//	1152 px
	public static final int WINDOW_HEIGHT = TILE_SIZE*GRID_Y;	//	768 px
	
	public static final Color colorV1 = Color.DARK_GRAY;
	public static final Color colorV2 = Color.DARK_GRAY;
	public static final Color colorV3 = Color.DARK_GRAY;
	public static final Color colorV4 = Color.DARK_GRAY;
	public static final Color colorV5 = Color.DARK_GRAY;//new Color(255, 162, 139, 200);
	public static final Color colorVT = new Color(0, 0, 0, 0);
}
