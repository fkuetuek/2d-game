package game;

/*
 * The Game class which updates the physic and renders the gameWindow.
 * Created by Yusuf developed by Fatma
 * 
 * @author	fatma, yusuf
 * @version	1.0
 * @since	2021-11-01
 * */

import engine.Input;


public class Game {
	private Input input = new Input();
	private PlayerInput playerInput;		
	private GameWindow gameWindow;
	private Physic physic;
	private GameState gameState;

	public Game() {
		playerInput = new PlayerInput(input);
		gameWindow = new GameWindow("2D Game", Config.WINDOW_WIDTH, Config.WINDOW_HEIGHT, input);
		physic = new Physic(playerInput);
		gameState = GameState.MENU;
	}


	public void update() {
		// close the game if the given key combination is pressed
		if (playerInput.keyExit()) {System.exit(0);}
		
		// Synchronize the state of the game, gameWindow and physic according to the key input 
		if (playerInput.keyPlay()) {
			gameState = GameState.RUNNING;
			gameWindow.setState(gameState);
			physic.setState(gameState);
		}
		
		// Synchronize the state of the game, gameWindow and physic according to the key input 
		if (playerInput.keyMenu()) {
			gameState = GameState.MENU;
			gameWindow.setState(gameState);
			physic.setState(gameState);
		}
		
		// Synchronize the state of the game, gameWindow and physic according to the key input 
		if (playerInput.keyRestart()) {
			// Enable restart only if the player lost the game (if the game is over)
			if (physic.physicState == GameState.GAME_OVER) {
				physic.restart();
				gameState = GameState.RUNNING;
				gameWindow.setState(gameState);
				physic.setState(gameState);
			}	
		}
		
		physic.update();
		physic.moveAndCollide();
	}

	// render the level, menu and game UI
	public void render() {
		gameWindow.render(physic);
	}


}




