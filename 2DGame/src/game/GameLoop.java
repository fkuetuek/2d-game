package game;

/*
 * The GameLoop class which updates and renders the Game with specified UPS and FPS.
 * It has a class member "private Game game;"
 * extends engine.GameLoop
 * 
 * @author	yusuf
 * @version	1.0
 * @since	2021-06-16
 * */

public class GameLoop extends engine.GameLoop {
	private Game game;	

	public GameLoop(int UPS, int FPS, boolean running) {
		super(UPS, FPS, running);
		game = new Game();

	}
	@Override
	public void run() {
		long initialTime = System.nanoTime();
		final double timeU = 1000000000 / UPS;
		final double timeF = 1000000000 / FPS;
		double deltaU = 0, deltaF = 0;
		long timer = System.currentTimeMillis();

		while (running) {

			long currentTime = System.nanoTime();
			deltaU += (currentTime - initialTime) / timeU;
			deltaF += (currentTime - initialTime) / timeF;
			initialTime = currentTime;

			if (deltaU >= 1) {
				update();
				deltaU--;
			}

			if (deltaF >= 1) {
				render();
				deltaF--;
			}

			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
			}
		}
	}

	@Override
	public void update() {
		game.update();
	}

	@Override
	public void render() {
		game.render();
	}

	public void setRunning(boolean running) {
		this.running = running;
	}
}










