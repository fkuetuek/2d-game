package game.enemies;

import java.awt.Rectangle;

import engine.Animation;
import engine.Vector2;
import game.Config;
import game.Enemy;
import game.GameObjState;


public class Fish extends Enemy {
	private Animation animation;
	
	public Fish(int x, int y, int speed, String color) {
		super(x, y, Config.TILE_SIZE, Config.TILE_SIZE, speed);
		collider =  new Rectangle(Config.TILE_SIZE/4, Config.TILE_SIZE/4, Config.TILE_SIZE/2, Config.TILE_SIZE/2);
		animation = new Animation("/fish_" + color + ".png", 4, 1, 200);
		currentFrame = animation.getFrame(0);
	}
	
	
	@Override
	public void update() {
		super.update();
		currentFrame = animation.getFrame(0);
	}
}
