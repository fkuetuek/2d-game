package game;

/*
 * an Enumerator class defines the game state constants
 * 
 * 
 * @author	yusuf, fatma
 * @version	1.0
 * @since	2021-06-16
 * */

public enum GameState {
	RUNNING,
	MENU,
	GAME_OVER;
}

