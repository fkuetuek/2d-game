package game;

import java.awt.event.KeyEvent;

import engine.Input;

/*
 * The PlayerInput class defines the keys and key combinations 
 * which controlls the game state, player and menu
 * implements "engine.PlayerInput"
 * keys and key combinations can be easily changed
 * 
 * @author	yusuf
 * @version	1.0
 * @since	2021-06-16
 * */

public class PlayerInput implements engine.PlayerInput {
	private Input input;

	public PlayerInput(Input input) {
		this.input = input; 
	}

	@Override
	public boolean keyUp() {
		return input.isPressed(KeyEvent.VK_UP) || input.isPressed(KeyEvent.VK_W);
	}

	@Override
	public boolean keyDown() {
		return input.isPressed(KeyEvent.VK_DOWN) || input.isPressed(KeyEvent.VK_S);
	}

	// prevents continuous usage of the attack key
	public boolean keyAttack() {
		boolean pressed = input.isPressed(KeyEvent.VK_X);
		input.setPressed(KeyEvent.VK_X, false);
		return pressed;
	}

	public boolean keyEscape() {
		return input.isPressed(KeyEvent.VK_ESCAPE);
	}

	
	public boolean keyRestart() {
		boolean pressed = input.isPressed(KeyEvent.VK_R);
		input.setPressed(KeyEvent.VK_R, false);
		return pressed;
	}
	
	public boolean keyPlay() {
		boolean pressed = input.isPressed(KeyEvent.VK_P);
		input.setPressed(KeyEvent.VK_P, false);
		return pressed;
	}

	public boolean keyExit() {
		return input.isPressed(KeyEvent.VK_Q);
	}

	public boolean keyMenu() {
		return input.isPressed(KeyEvent.VK_M);
	}

}
