package game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.Rectangle;

import engine.Vector2;


/*
 * Base abstract class for Player and Enemy classes
 * Updated by Fatma
 * 
 * @author	yusuf, fatma
 * @version	1.0
 * @since	2021-06-16
 * */


public abstract class GameObject {
	protected int speed;
	protected Vector2 position;
	protected Vector2 size;
	protected Vector2 direction;
	protected Rectangle collider;
	protected boolean colliding;
	protected boolean moving;
	protected GameObjState state; 

	protected Graphics2D combinedImage;
	protected BufferedImage currentFrame;
	protected BufferedImage sprite;
	protected GameObjType type;


	public GameObject(int x, int y, int width, int height, int speed, GameObjType type) {
		this.speed = speed;
		position = new Vector2(x, y);
		size = new Vector2(width, height);
		direction = new Vector2(0, 1);
		colliding = false;
		moving = false;
		state = GameObjState.SWIM;
		this.type = type;
		currentFrame = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
	}

	public abstract void update();
	public abstract boolean isEnemy();

	public Vector2 getDirection() {return direction;}
	public GameObjState getState() {return state;}
	public Vector2 getPosition() {return position;}
	
	public void setDirection(Vector2 vec) {this.direction = vec;}
	public void setState(GameObjState state) {this.state = state;}
	public void setColliding(boolean colliding) {
		this.colliding = colliding;
	}
	
	public void resetDirection() {direction.vectorZero2();}
	

	public Image getImage() {
		sprite = new BufferedImage(
				size.x,
				size.y,
				BufferedImage.TYPE_INT_ARGB);

		combinedImage = sprite.createGraphics();

		// Draw collision rectangles if DEBUG is true
		if (Config.DEBUG) {
			combinedImage.setColor(new Color(0, 255, 0, 125));
			combinedImage.fillRect(
					0,
					0,
					size.x, 
					size.y);
			combinedImage.fillOval(0, 0, 10, 10);
			combinedImage.fillRect(
					collider.x,
					collider.y,
					collider.width, 
					collider.height);
		}
		
		combinedImage.drawImage(currentFrame, 0, 0, size.x, size.y, null);
		combinedImage.dispose();
		return sprite;
	}


	public void moveH() {
		if (moving) {
			position.x -= speed;
		}
	}

	public void moveV() {
		if (moving) {
			position.y += speed*direction.y;
		}
	}

	// Check collision
	public boolean checkCollision(Vector2 pos, Rectangle col) {
		return
				position.x + collider.x < pos.x + col.x + col.width &&
				position.x + collider.x + collider.width > pos.x + col.x &&
				position.y + collider.y < pos.y + col.y + col.height &&
				position.y + collider.y + collider.height > pos.y + col.y;
	}

	


}
