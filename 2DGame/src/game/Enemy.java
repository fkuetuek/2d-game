package game;

/*
 * Base class for Enemy classes
 * it extends GameObject class
 * updated by Fatma
 * 
 * @author	yusuf, fatma
 * @version	1.0 
 * @since	2021-06-16
 * */

public class Enemy extends GameObject {
	public Enemy(int x, int y, int width, int height, int speed) {
		super(x, y, width, height, speed, GameObjType.ENEMY);
	}

	public boolean isEnemy() {
		return true;
	}


	@Override
	public void update() {
		
	}
	
	public boolean attackingPlayer(Player p) {
		if (this.checkCollision(p.position, p.collider)) {
			return true;
		} else {
			return false;
		}
	}


}
