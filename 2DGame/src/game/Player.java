package game;

import java.awt.Rectangle;

import engine.Animation;
import engine.Vector2;

/*
 * the Player class defines the playable chracter of the Game
 * extends abstract GameObject
 * 
 * 
 * @author	yusuf, fatma
 * @version	1.0
 * @since	2021-06-16
 * */

public class Player extends GameObject {
	private Animation[] animations = new Animation[3];
	private PlayerInput playerInput;
	private int animationRow;
	private int animationNumber;
	private boolean attacking = false;

	private long start;
	private long attackTimer = 300;
	private int lives = 3;
	private int score = 0;
	private int lifeSpan = 100;

	public Player(int x, int y, int width, int height, int speed, PlayerInput playerInput) {
		super(x, y, width, height, speed, GameObjType.PLAYER);
		int tileSize = width;
		collider = new Rectangle(tileSize/4, tileSize/4+20, tileSize/2, tileSize/2);
		animations[0] = new Animation("/run-sheet-1.png", 6, 1, 140);
		animations[1] = new Animation("/attack.png", 6, 1, (int) attackTimer/2);
		direction.set(0, 0);
		this.playerInput = playerInput;
		state = GameObjState.SWIM;
		start = 0;
	}

	public int getLives() { return lives; }

	public int getScore() { return score; }

	public int getLifeSpan() { return lifeSpan; }

	public void setLives(int lives) { this.lives = lives; }

	public void setScore(int score) { this.score = score; }

	public void increaseScore(int score) { this.score += score; }

	public void setLifeSpan(int lifeSpan) { this.lifeSpan = lifeSpan; }

	public void processInput() {
		long now = System.currentTimeMillis();
		attacking=false;
		moving = false;

		if (!attacking&&(playerInput.keyDown() || playerInput.keyUp())) {
			moving = true;
			direction.x = 0;
			direction.y = 0;
			state = GameObjState.SWIM;
		}
		if (playerInput.keyUp()) {
			direction.setY(-1);
		} 

		if (playerInput.keyDown()) {
			direction.setY(1);
		} 

		if (playerInput.keyAttack()) {
			start = now;
			attacking=true;
			moving = false;
			state = GameObjState.ATTACK;			
		}

		if (!moving && !attacking) {
			if (now - start >= attackTimer) {
				state = GameObjState.SWIM;
			}

		}

	}

	public void updateAnimation() {
		if (state == GameObjState.SWIM) {
			animationNumber = 0;
			animations[1].reset();
		}		
		else if (state == GameObjState.SWIM) {
			animationNumber = 1;
			animations[1].reset();
		}
		else if (state == GameObjState.ATTACK) {

			animationNumber = 1;
		}

	}

	public boolean isEnemy() {
		return false;
	}


	@Override
	public void update() {
		processInput();
		updateAnimation();

		animationRow = 0; 

		currentFrame = animations[animationNumber].getFrame(animationRow);
	}

	public void resetPlayer () {
		this.lifeSpan = 100;
		this.lives = 3;
		this.score = 0;
		this.position.y = Config.WINDOW_HEIGHT/2 - Config.TILE_SIZE/2;
	}


}
