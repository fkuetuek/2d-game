package game;

/*
 * An Enumerator class defines the states of game objects
 * 
 * @author	yusuf
 * @version	1.0
 * @since	2021-06-16
 * */


public enum GameObjState {
	SWIM,
	ATTACK
}
