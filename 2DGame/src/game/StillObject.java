package game;

import java.awt.Rectangle;

import engine.Animation;

public class StillObject extends GameObject {
	private Animation animation;
	public StillObject(int x, int y, int speed, GameObjType type, String pathToSprite, int gridX, int gridY, int animSpeed) {
		super(x, y, Config.TILE_SIZE, Config.TILE_SIZE, speed, type);
		collider =  new Rectangle(Config.TILE_SIZE/4, Config.TILE_SIZE/4, Config.TILE_SIZE/2, Config.TILE_SIZE/2);
		animation = new Animation(pathToSprite, gridX, gridY, animSpeed);
		currentFrame = animation.getFrame(0);
	}

	public StillObject(int x, int y, int width, int height, int speed, GameObjType type, String pathToSprite, int gridX, int gridY, int animSpeed) {
		super(x, y, width, height, speed, type);
		collider =  new Rectangle(Config.TILE_SIZE/8, Config.TILE_SIZE/8, Config.TILE_SIZE/4, Config.TILE_SIZE/4);
		animation = new Animation(pathToSprite, gridX, gridY, animSpeed);
		currentFrame = animation.getFrame(0);
	}

	@Override
	public void update() {
		currentFrame = animation.getFrame(0);
		
	}

	@Override
	public boolean isEnemy() {
		return false;
	}

	

}
