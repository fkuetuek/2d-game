package game;

/*
 * The Main class is the entry point to the game.
 * 
 * @author	fatma
 * @version	1.0
 * @since	2021-10-18
 * */

public class Main {

	private game.GameLoop gameLoop;

	public Main() {
		gameLoop = new game.GameLoop(Config.UPS, Config.FPS, true);
		gameLoop.run();
	}

	public static void main(String[] args) {
		new Main();

	}

}
