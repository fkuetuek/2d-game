package game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.io.IOException;

import javax.imageio.ImageIO;

import engine.Input;
import engine.Vector2;

import java.awt.image.BufferedImage;
import java.awt.Graphics2D;

/*
 * The GameWindow draws several types(GameObject, Tile, Menu) of objects to the window
 * It takes Game as a parameter and draws objects according to game state
 * Some part of the codes are taking from the Java 2D Game which was developed on 2021-06-10
 * by Fatma Sude Kütük and Yusuf Kütük
 * 
 * @author	Fatma Sude Kütük, Yusuf Kütük
 * @version	1.0
 * @since	2021-10-20
 * */

public class GameWindow extends engine.Window {

	private static final long serialVersionUID = -5384553046664110020L;
	private GameState gameWindowState;
	private BufferedImage img = null;
	private BufferedImage bg = null;
	private BufferedImage play = null;
	private BufferedImage restart = null;
	private BufferedImage menu = null;
	private BufferedImage quit = null;
	private BufferedImage gameOver = null;
	private Graphics2D graphics;
	private BufferStrategy bufferStrategy;
	private Font font = new Font("Serif", Font.PLAIN, 50);
	private Font font1 = new Font("Serif", Font.PLAIN, 60);

	public GameWindow(String title, int width, int height, Input input) {
		super(title, width, height);
		gameWindowState  = GameState.MENU;

		addKeyListener(input);

		// Load images
		try {
			img = ImageIO.read(getClass().getResource("/rip.png"));
			bg = ImageIO.read(getClass().getResource("/bg_grad.png"));
			play = ImageIO.read(getClass().getResource("/play.png"));
			restart = ImageIO.read(getClass().getResource("/restart1.png"));
			menu = ImageIO.read(getClass().getResource("/menu.png"));
			quit = ImageIO.read(getClass().getResource("/quit.png"));
			gameOver = ImageIO.read(getClass().getResource("/game_over.png"));
		} catch (IOException e) {
			System.err.println("sprite sheet is not loaded");
			e.printStackTrace();
		}


	}


	public void render(Physic physic) {
		bufferStrategy = canvas.getBufferStrategy();
		graphics = (Graphics2D) bufferStrategy.getDrawGraphics();
		String score = physic.player.getScore() + "";

		graphics.drawImage(bg, 0, 0, Config.WINDOW_WIDTH, Config.WINDOW_HEIGHT, null);

		if (physic.physicState == GameState.RUNNING) {
			gameWindowState = GameState.RUNNING;
		}

		if (physic.physicState == GameState.MENU) {
			gameWindowState = GameState.MENU;
		}

		if (physic.physicState == GameState.GAME_OVER) {
			gameWindowState = GameState.GAME_OVER;
		}


		if (gameWindowState == GameState.RUNNING) {

			// Game objects including player
			for (int i = 0; i < physic.getGameObjects().size(); ++i) {
				GameObject g = physic.getGameObjects().get(i);
				graphics.drawImage(g.getImage(), g.getPosition().x,	g.getPosition().y, null);
			}

			// draw player's lives
			for (int i = 1; i <= physic.player.getLives(); i++) {
				int posX = 10 + (i-1)*70;
				graphics.drawImage(physic.life, posX, 0, null);
			}

			//draw player's score
			graphics.drawImage(physic.score, 500, 0, null);
			graphics.setFont(font);
			graphics.setColor(Color.BLACK);
			graphics.drawString(score, 600, 66);
			
		} else if (gameWindowState == GameState.MENU) {

			for (int i = 0; i < physic.getMenuObjects().size(); ++i) {
				GameObject m = physic.getMenuObjects().get(i);

				graphics.drawImage(m.getImage(), m.getPosition().x, m.getPosition().y, null);

				graphics.drawImage(play, Config.TILE_SIZE * 6, Config.TILE_SIZE * 2 ,Config.TILE_SIZE*4, Config.TILE_SIZE*2, null);
				graphics.drawImage(menu, Config.TILE_SIZE * 6, Config.TILE_SIZE * 3 ,Config.TILE_SIZE*4, Config.TILE_SIZE*2, null);
				graphics.drawImage(quit, Config.TILE_SIZE * 6, Config.TILE_SIZE * 4 ,Config.TILE_SIZE*4, Config.TILE_SIZE*2, null);
			}
			
		} else {
			graphics.setFont(font1);
			graphics.setColor(Color.BLACK);
			graphics.drawString("Score: " + score, Config.TILE_SIZE * 7, Config.TILE_SIZE * 6);
			graphics.drawImage(gameOver, Config.TILE_SIZE*4, Config.TILE_SIZE/3, Config.TILE_SIZE *9, Config.TILE_SIZE *3, null);			
			graphics.drawImage(img, Config.TILE_SIZE, Config.TILE_SIZE, Config.TILE_SIZE *6, Config.TILE_SIZE *6, null);
			graphics.drawImage(restart, Config.TILE_SIZE * 6, Config.TILE_SIZE * 3 ,Config.TILE_SIZE*4, Config.TILE_SIZE*2, null);
			graphics.drawImage(quit, Config.TILE_SIZE * 6, Config.TILE_SIZE * 4 ,Config.TILE_SIZE*4, Config.TILE_SIZE*2, null);
		}

		graphics.dispose();
		bufferStrategy.show();
	}


	public void setState(GameState state) {
		gameWindowState = state;
	}



}



