package game;
/*
 * An Enumerator class defines the types of the game objects
 * created by Yusuf developed by Fatma
 * @author	fatma, yusuf
 * @version	1.0
 * @since	2021-10-16
 * */
public enum GameObjType {
	PLAYER,
	ENEMY,
	OBJECT,
	COIN,
	LIFE,
	MENU
}
