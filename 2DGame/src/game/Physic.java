package game;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;

import engine.Clock;
import engine.RandomNumber;
import game.enemies.Fish;



public class Physic {

	ArrayList<GameObject> gameObjects = new ArrayList<GameObject>();
	ArrayList<GameObject> menuObjects = new ArrayList<GameObject>();
	Player player = null;
	Clock clock1;
	Clock clock2;
	Clock clock3;
	Random rand1;
	RandomNumber rand;
	RandomNumber randFish;
	int random;
	protected BufferedImage life;
	protected BufferedImage score;
	StillObject menuCharacter;
	GameState physicState;
	PlayerInput input;


	public Physic(PlayerInput input) {
		this.input = input;
		physicState = GameState.MENU;
		player = new Player(
				Config.TILE_SIZE/10, 
				Config.WINDOW_HEIGHT/2 - Config.TILE_SIZE/2, 
				Config.TILE_SIZE * 2, 
				Config.TILE_SIZE * 2, 
				5,
				input);
		clock1 = new Clock();
		clock2 = new Clock();
		clock3 = new Clock();
		rand = new RandomNumber();
		randFish = new RandomNumber();
		rand1 =  new Random();
		loadImages();
		gameObjects.add(player);
//		gameObjects.add(new Fish(Config.WINDOW_WIDTH, 200, 3, "pink"));
//		gameObjects.add(new StillObject(Config.WINDOW_WIDTH, Config.WINDOW_HEIGHT - 88, 3, GameObjType.COIN, "/water_plant.png", 2, 1, 400));
		menuObjects.add(new StillObject(Config.TILE_SIZE, Config.TILE_SIZE, Config.TILE_SIZE * 4, Config.TILE_SIZE * 4,  0, GameObjType.COIN, "/run.png", 6, 1, 300));
		//menuObjects.add(new StillObject(Config.WINDOW_WIDTH, random * Config.TILE_SIZE, 3, GameObjType.LIFE, "/life.png", 6, 1, 200));


	}

	public ArrayList<GameObject> getGameObjects() {
		return gameObjects;
	}

	public ArrayList<GameObject> getMenuObjects() {
		return menuObjects;
	}

	public void update() {

		player.processInput();

		if (player.getLives() == 0) {
			physicState = GameState.GAME_OVER;
		}
		

		if (physicState == GameState.RUNNING) {
			generateGameObjs();

			for (int i = 1; i < gameObjects.size(); ++i) {

				if (player.checkCollision(gameObjects.get(i).position, gameObjects.get(i).collider) && player.getState() == GameObjState.ATTACK && gameObjects.get(i).isEnemy()) {
					player.increaseScore(5);
					gameObjects.remove(i);
				}

				if (player.checkCollision(gameObjects.get(i).position, gameObjects.get(i).collider) && !(gameObjects.get(i).isEnemy())) {
					if (gameObjects.get(i).type == GameObjType.COIN) {
						player.increaseScore(10);	
					}
					if (gameObjects.get(i).type == GameObjType.LIFE && player.getLives() < 3) {
						player.setLives(player.getLives() + 1);
						player.setLifeSpan(100);
					}
					if (gameObjects.get(i).type == GameObjType.OBJECT) {

					} else {
						gameObjects.remove(i);
					}

				}

				if (player.checkCollision(gameObjects.get(i).position, gameObjects.get(i).collider) 
						&& !(player.getState() == GameObjState.ATTACK) 
						&& gameObjects.get(i).isEnemy()) {

					player.setLifeSpan(player.getLifeSpan() - 2);

					if (player.getLifeSpan() == 0 && player.getLives() > 0) {
						player.setLives(player.getLives() - 1);
						player.setLifeSpan(100);
					}
				}
				if (gameObjects.get(i).getPosition().x < -(gameObjects.get(i).size.x)) {
					gameObjects.remove(i);
					
				}
			}	
		}

	}


	public void moveAndCollide() {

		if (physicState == GameState.RUNNING) {

			for (int i = 0; i < gameObjects.size(); ++i) {
				GameObject g = gameObjects.get(i);	

				g.update();
				if ((g instanceof Player)) {
					g.moveV();
					if (player.getPosition().y < 60) {
						player.position.y = 61;
					}
					if (player.getPosition().y > Config.WINDOW_HEIGHT - 121 - player.collider.height) {
						player.position.y = Config.WINDOW_HEIGHT - 120 - player.collider.height;
					}
				} else {
					g.moving = true;
					g.moveH();
				}

			}
		}

		if (physicState == GameState.MENU) {

			for (int i = 0; i < menuObjects.size(); ++i) {
				GameObject m = menuObjects.get(i);
				m.update();
			}
		}

	}

	public void loadImages() {
		try {
			life = ImageIO.read(getClass().getResource("/player_life.png"));
			score = ImageIO.read(getClass().getResource("/player_score.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Generate Game objects randomly
	public void generateGameObjs() {
		// Generate fishes in every 4 seconds
		if (clock1.nSecond(4)) {
			random = rand.randomize(1, Config.GRID_Y - 2);
			int i = randFish.randomize(1, 3);
			if (i == 1) {
				gameObjects.add(new Fish(Config.WINDOW_WIDTH, random * Config.TILE_SIZE, 3, "pink"));
			} else if (i == 2) {
				gameObjects.add(new Fish(Config.WINDOW_WIDTH, random * Config.TILE_SIZE, 3, "red"));
			} else if (i == 3) {
				gameObjects.add(new Fish(Config.WINDOW_WIDTH, random * Config.TILE_SIZE, 3, "yellow"));
			}
			clock1.reset();
		}
		
		// Generate bonuses in every 8 seconds
		if (clock2.nSecond(8)) {
			random = rand.randomize(1, Config.GRID_Y - 2);
			int r = rand1.nextInt(5);
			if (r == 1) 
			{
				gameObjects.add(new StillObject(Config.WINDOW_WIDTH, random * Config.TILE_SIZE, 3, GameObjType.LIFE, "/life.png", 6, 1, 200));
			} else {
				gameObjects.add(new StillObject(Config.WINDOW_WIDTH,  random * Config.TILE_SIZE, 3, GameObjType.COIN, "/coin.png", 6, 1, 200));
			}
			clock2.reset();
		}
		
		// Generate water plants in every 5 seconds
		if (clock3.nSecond(5)) {
			random = rand1.nextInt(Config.GRID_Y - 1) + 1;
			gameObjects.add(new StillObject(Config.WINDOW_WIDTH, Config.WINDOW_HEIGHT - 100, 3, GameObjType.OBJECT, "/water_plant.png", 2, 1, 200));
			clock3.reset();
		}
	}

	// restart the game
	public void restart () {
		gameObjects = null;
		gameObjects = new ArrayList<GameObject>();
		player.resetPlayer();
		gameObjects.add(player);
	}

	public void setState(GameState state) {
		physicState = state;
	} 


}
