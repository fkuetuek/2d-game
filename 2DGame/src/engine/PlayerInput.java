package engine;

/*
 * Simple interface for defining PlayerInput classes.
 * It only has keys for directions but can be extended easily
 * 
 * @author	yusuf, fatma
 * @version	1.0
 * @since	2021-06-16
 * */


public interface PlayerInput {

	public boolean keyUp();
	public boolean keyDown();
}
