package engine;

/*
 * Very basic game loop which does not have any control over UPS and FPS
 * implements java.lang.Runnable
 * 
 * @author	yusuf
 * @version	1.0
 * @since	2021-06-16
 * */

public abstract class GameLoop implements Runnable {
	protected int UPS;
	protected int FPS;
	protected boolean running;

	public GameLoop(int UPS, int FPS, boolean running) {
		this.UPS = UPS;
		this.FPS = FPS;
		this.running = running;
	}

	@Override
	public void run() {
		int frames = 0, ticks = 0;
		long timer = System.currentTimeMillis();

		while (running) {
			update();
			ticks++;
			render();
			frames++;
			if (System.currentTimeMillis() - timer > 1000) {
				if (true) {
					System.out.println(String.format("UPS: %s, FPS: %s", ticks, frames));	
				}
				frames = 0;
				ticks = 0;
				timer += 1000;
			}
		}
	}
	
	public abstract void update();
	
	public abstract void render();



	
}
