package engine;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

/*
 * Very basic class for defining and playing sprite sheet animations
 * Created by Yusuf on 2021-06-16 for Game Programming course (WS 2021)
 * developed by Fatma Sude Kütük
 * @author	fatma, yusuf
 * @version	1.0
 * @since	2021-06-16
 * */

public class Animation {
	private BufferedImage sprite;
	private int tileWidth;// = 48;
	private int tileHeight;// = 48;
	private int gridX;//= 6;
	private int gridY;// = 8;
	private int speed = 30;
	private boolean running = true;
	private int currentFrame = 0;
	private int frameTick = 0;

	
	public int getSpeed() {
		return speed;
	}

	
	public void setSpeed(int frameMillis) {
		this.speed = 30 / (1000 / frameMillis);
	}

	
	public Animation (String path, int gridX, int gridY, int frameMillis) {
		try {
			sprite = ImageIO.read(getClass().getResource(path));
			this.gridX = gridX;
			this.gridY = gridY;
			tileWidth = (sprite.getWidth() / gridX);
			tileHeight = (sprite.getHeight() / gridY);
			this.speed = 30 / (1000 / frameMillis);
		} catch (IOException e) {
			System.err.println("sprite sheet is not loaded");
			e.printStackTrace();
		}
	}

	public int getWidth() {return sprite.getWidth();}
	public int getTileWidth() {return tileWidth;}
	public int getTileHeigt() {return tileHeight;}
	public int getGridX() {return gridX;}
	public int getGridY() {return gridY;}
	public BufferedImage getSprite() {return sprite;}
	public int getCurrentframe() {return currentFrame;}
	public void setCurrentFrame(int f) {currentFrame = f;}

	public BufferedImage getFrame(int row) {
		if (running) {
			frameTick++;
			if (frameTick >= speed) {
				if (currentFrame < gridX - 1) {
					currentFrame++;
				} else {
					currentFrame = 0;
				}
				frameTick = 0;
			}
		}


		return sprite.getSubimage(currentFrame*tileWidth, row*tileHeight, tileWidth, tileHeight);
	}



	public void reset() {currentFrame=0;}
	

}









