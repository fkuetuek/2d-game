package engine;
import java.util.Random;

/*
 * Very basic class for generating random numbers between defined range
 * This class prevents generating the same number successively
 * @author	Fatma
 * @version	1.0
 * @since	2021-11-06
 * */

public class RandomNumber {
	Random random;
	int randomNumber;
	int previousRandom;
	
	public RandomNumber() {
		random = new Random();
		previousRandom = 0;
	}
	
	public int randomize(int min, int max) {
		randomNumber = random.nextInt(max) + min;
		while (randomNumber == previousRandom) {
			randomNumber = random.nextInt(max) + min;
		}
		previousRandom = randomNumber;
		return randomNumber;
	}
}
