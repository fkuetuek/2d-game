package engine;

/*
 * helper class for the movement of game objects
 * 
 * @author	yusuf, fatma
 * @version	1.0
 * @since	2021-06-16
 * */

public class Vector2 {
	public int x;
	public int y;

	public Vector2(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void vectorZero2() {
		x = 0;
		y = 0;
	}
	
	
/* this method is designed for 8 way animation. 
 * but this game animations have only one way
 * therefore unnecessary parts are commented
 */
	public int direction() {
		return 0;
//		if 		(x == 0 && y > 0)	{return 0;}
//		else if (x > 0 && y > 0) 	{return 1;}
//		else if (x > 0 && y == 0) 	{return 2;}
//		else if (x > 0 && y < 0) 	{return 3;}
//		else if (x == 0 && y < 0) 	{return 4;}
//		else if (x < 0 && y < 0) 	{return 5;}
//		else if (x < 0 && y == 0) 	{return 6;}
//		else if (x < 0 && y > 0) 	{return 7;}
//		else 						{return 0;}
	}

	public void set(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {return x;}
	public void setX(int x) {this.x = x;}

	public int getY() {return y;}
	public void setY(int y) {this.y = y;}
	public double length() {return Math.sqrt(x*x + y*y);}
	public void opposite() {
		x= -x;
		y= -y;
	}

	public void oppositeX() {
		x= -x;
	}

	public void oppositeY() {
		y= -y;
	}


	public int getVectorDirectionX() {
		if (this.x > 0) {
			return 1;
		} else if (this.x < 0) {
			return - 1;
		} else {
			return 0;
		}
	}

	public int getVectorDirectionY() {
		if (this.y > 0) {
			return 1;
		} else if (this.y < 0) {
			return - 1;
		} else {
			return 0;
		}
	}


	@Override
	public String toString() {
		return "Vector2 [x=" + x + ", y=" + y + "]";
	}

}









