package engine;

/*
 * A simple class used as a timer.
 * Created by Yusuf on 2021-06-16 for Game Programming course (WS 2021)
 * developed by Fatma Sude Kütük
 * @author	fatma, yusuf
 * @version	1.0
 * @since	2021-06-16
 * */

public class Clock {
	private long start;
	private boolean running = false;
	private double stop = 0.0;
	private boolean timer = false;

	public Clock() {
		start = System.currentTimeMillis();
		start();
	}

	public double getSecond() {
		long now = System.currentTimeMillis();
		if (!running) {
			return stop;
		}
		return stop + ((now - start) / 1000.0);
	}

	public void stop() {
		if (running) {
			stop = getSecond();
			running = false;
		}
	}

	public void start() {
		if (!running) {
			running = true;
			start = System.currentTimeMillis();
		}
	}
	
	public void reset() {
		stop = 0.0;
		stop();
		start();
	}
	
	public boolean nSecond(int n) {
		timer = false;
		if ((int)getSecond() >= n) {
			reset();
			timer = true;
		}
		return timer; 
	}
	
}
