package engine;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/*
 * the Input class implements java.awt.event.KeyListener
 * 
 * @author	yusuf
 * @version	1.0
 * @since	2021-06-16
 * */

public class Input implements KeyListener {

	private boolean[] pressed;
	
	public Input() {
		pressed = new boolean[255];
	}
	
	public boolean isPressed(int keyCode) {
		return pressed[keyCode];
	}
	
	public void setPressed(int keycode, boolean b) {
		pressed[keycode] = b;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		pressed[e.getKeyCode()] = true;
	}

	@Override
	public void keyReleased(KeyEvent e) {
		pressed[e.getKeyCode()] = false;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

}
